package org.classicmayan.tglab.teiparser.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.WorkbenchException;

import org.classicmayan.tglab.teiparser.core.Activator;

public class ShowTEIParserPerspectiveHandler extends AbstractHandler implements IHandler{

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		try {
						
			// first reset the perspective...
			IWorkbench wb = PlatformUI.getWorkbench();
			wb.getActiveWorkbenchWindow().getActivePage().setPerspective(
					wb.getPerspectiveRegistry().findPerspectiveWithId(
							"org.classicmayan.tglab.teiparser.perspectives.parserPerspective"));
			wb.getActiveWorkbenchWindow().getActivePage().resetPerspective();

			// show it when necessary
			PlatformUI.getWorkbench().showPerspective(
					"org.classicmayan.tglab.teiparser.perspectives.parserPerspective",
					PlatformUI.getWorkbench().getActiveWorkbenchWindow());
			wb.getIntroManager().closeIntro(wb.getIntroManager().getIntro());
					
		} catch (WorkbenchException e) {
			IStatus status = new Status(IStatus.ERROR, Activator.PLUGIN_ID,
				    "Could not open TEIParser perspective!", e);
			Activator.getDefault().getLog().log(status);
		} 
		return null;
	}
}
