package org.classicmayan.tglab.teiparser.core;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;

import javax.ws.rs.core.Response;

import org.apache.cxf.helpers.IOUtils;
import org.apache.cxf.jaxrs.client.WebClient;
import org.apache.cxf.jaxrs.ext.form.Form;
import org.classicmayan.tglab.teiparser.preferences.PluginPreferencePage;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;


import info.textgrid.lab.authn.RBACSession;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.ui.core.dialogs.INewObjectPreparator;
import info.textgrid.lab.ui.core.dialogs.ITextGridWizard;
import info.textgrid.lab.ui.core.dialogs.NewObjectWizard;
import info.textgrid.lab.ui.core.menus.OpenObjectService;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.progress.UIJob;

/**
 * Executed by click menu.<br/>
 */
public class SampleHandler extends AbstractHandler implements INewObjectPreparator{

    private final IWorkbenchWindow window;
    private ITextGridWizard wizard;
    

    /**
     * constructor.
     */
    public SampleHandler() {
        IWorkbench workbench = PlatformUI.getWorkbench();
        this.window = workbench.getActiveWorkbenchWindow();
    }

	@Override
	public void setWizard(ITextGridWizard wizard) {
		// TODO Auto-generated method stub
		this.wizard = wizard;
	}
	
	@Override
	public void initializeObject(TextGridObject textGridObject) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public boolean performFinish(final TextGridObject textGridObject) {
		
		new Job("creating teiparser object"){

			@Override
			protected IStatus run(IProgressMonitor arg0) {
				try {
					
			    	String url = Platform.getPreferencesService().getString(Activator.PLUGIN_ID, PluginPreferencePage.classicmayanTeiParserUrl_id, "", null);
					Form form = new Form();
					form.set("tgSID", RBACSession.getInstance().getSID(false));
					form.set("tgPID", textGridObject.getProject());
					form.set("title", textGridObject.getTitle());
			    	Response res = WebClient.create(url + "/teiparser/teiparser/parse").form(form);
			    	
			    	if(res.getStatus() != javax.ws.rs.core.Response.Status.CREATED.getStatusCode()) {
			    		// TODO
			    	}
			    	
			    	InputStream in = (InputStream)res.getEntity();
					JSONObject obj = new JSONObject(IOUtils.toString(in));
					URI textgridUri = new URI((String)obj.get("@id"));
					
					final TextGridObject tgo = TextGridObject.getInstance(textgridUri, true);
					
					new UIJob("opening editor") {
						
						@Override
						public IStatus runInUIThread(IProgressMonitor arg0) {
							OpenObjectService.getInstance().openObject(tgo, 0, false);
							return Status.OK_STATUS;
						}
					}.schedule();
					
				} catch (CoreException e) {
					Activator.handleError(e, "error parsing TEI file");
				} catch (IOException e) {
					Activator.handleError(e, "error parsing TEI file");
				} catch (JSONException e) {
					Activator.handleError(e, "error parsing TEI file");
				} catch (URISyntaxException e) {
					Activator.handleError(e, "error parsing TEI file");
				}
				
				return Status.OK_STATUS;
			}
			
		}.schedule();
		
		if (wizard instanceof NewObjectWizard)
			return true;
		else
			return false;				
		
	}

	
	
    /**
     * {@inheritDoc}
     */
    @Override
    public Object execute(ExecutionEvent event) throws ExecutionException {
        MessageDialog.openInformation(window.getShell(), "Eclipse Plugin Archetype", "Hello, Maven+Eclipse world,\n teiparser is built with Tycho");
        return null;
    }

}
