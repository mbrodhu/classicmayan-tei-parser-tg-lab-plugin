package org.classicmayan.tglab.teiparser.perspective;

import org.eclipse.ui.IFolderLayout;
import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;

public class TEIparserPerspective implements IPerspectiveFactory{
	
	@Override
	public void createInitialLayout(IPageLayout layout) {
		// TODO Auto-generated method stub
		
    	String editorArea = layout.getEditorArea();
    	
    	IFolderLayout topLeft = 
    		layout.createFolder("topLeft", IPageLayout.LEFT, (float)0.3, editorArea);//$NON-NLS-1$
       	topLeft.addView("info.textgrid.lab.navigator.view"); 
	
    	layout.setEditorAreaVisible(true); 
	}

}
