package org.classicmayan.tglab.teiparser.preferences;

import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;
import org.eclipse.jface.preference.IPreferenceStore;

import org.classicmayan.tglab.teiparser.core.Activator;

public class PluginPreferencePageInitializer extends AbstractPreferenceInitializer{

	@Override
	public void initializeDefaultPreferences() {
		IPreferenceStore store = Activator.getDefault().getPreferenceStore();

		store.setDefault(PluginPreferencePage.classicmayanTeiParserUrl_id, "http://localhost:3131/");

	}
}
