package org.classicmayan.tglab.teiparser.preferences;

import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.preference.StringFieldEditor;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;

import org.classicmayan.tglab.teiparser.core.Activator;

public class PluginPreferencePage extends FieldEditorPreferencePage implements IWorkbenchPreferencePage{

	public static String classicmayanTeiParserUrl_id = "classicmayanTeiParser_url";
	
	@Override
	protected void createFieldEditors() {

		addField(new StringFieldEditor(classicmayanTeiParserUrl_id, "URL to Classicmayan TEI-Parser Location", getFieldEditorParent()));
		
	}
	
	@Override
	public void init(IWorkbench workbench) {

		setPreferenceStore(Activator.getDefault().getPreferenceStore());
		
	}
}
