package org.classicmayan.tglab.teiparser.views;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IPersistableElement;

import info.textgrid.lab.core.model.TextGridObject;

public class ParserEditorInput implements IEditorInput {
	
	private TextGridObject tgo;
	
	public ParserEditorInput (TextGridObject tgo) {
		this.tgo = tgo;
	}
	
	public TextGridObject getTGO() {
		return this.tgo;
	}

	@SuppressWarnings("rawtypes")
	public Object getAdapter(Class adapter) {
		if (adapter.equals(TextGridObject.class))
			return this.tgo;
		return null;
	}
	
	public boolean exists() {
		return false;
	}
	
	public ImageDescriptor getImageDescriptor() {
		return null;
	}
	
	public String getName() {
		try {
			return tgo.getTitle();
		} catch (CoreException e) {
			return null;
		}
	}
	
	public IPersistableElement getPersistable() {
		return null;
	}
	
	public String getToolTipText() {
		String string;
		try {
			string = getName() + "; " + tgo.getContentTypeID() + "; " + tgo.getURI().toString();
		} catch (CoreException e) {
			return getName();
		}
		return string;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof ParserEditorInput) {
			if (((ParserEditorInput)obj).tgo.equals(tgo))
				return true;
		}
		return false;
	}
	
}
